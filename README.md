# ipa.sh

Repo for website [ipa.sh](https://ipa.sh).

Shows your external IP to you, very useful to me as a sysadmin. I often use ifconfig.me but it was slow for a while so I registered ipa.sh to have a service of my own I could trust.

## Zappa setup

    $ zappa init

### DNS & Cert

I made sure to have a domain in AWS already, requested a cert for it through ACM.

## Zappa deploy & certify

Updated ``zappa_settings.json`` with the ARN of the domain.

```json
{
    "production": {
        "app_function": "ipash.app",
        "aws_region": "eu-north-1",
        "profile_name": "default",
        "project_name": "ipa-sh",
        "runtime": "python3.8",
        "s3_bucket": "zappa-xxxxx",
        "certificate_arn": "arn:aws:acm:us-east-1:7xxxxx:certificate/xxxx",
        "domain": "ipa.sh"
    }
}
```

    $ zappa deploy production
    $ zappa certify production

## See also

* [Based on this blog post](https://romandc.com/zappa-django-guide/walk_domain/#option-1-route53-and-acm)
