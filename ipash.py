from flask import Flask, request, Response

app = Flask(__name__)


@app.route('/')
def return_ip():
    res_data = {}
    res_data['ip'] = request.remote_addr

    response = Response(res_data['ip'])
    response.headers['Content-type'] = 'text/plain'
    return response


if __name__ == '__main__':
    app.run()
